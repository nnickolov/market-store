﻿using System.Text;

namespace Market_Store
{
    public class BronzeCard : Card
    {
        public override string DiscountByCard(double turnOver, double purchaseValue)
        {
            var sb = new StringBuilder();

            if (turnOver < 100)
            {
                this.DiscountRate = 1;
            }

            else if (turnOver >= 100 && turnOver <= 300)
            {
                this.DiscountRate = 0.01;
            }

            else
            {
                this.DiscountRate = 0.025;
            }

            if (this.DiscountRate == 1)
            {
                this.DiscountRate = 0;
                this.PurchaseValue = purchaseValue;
                this.Total = PurchaseValue - (PurchaseValue * this.DiscountRate);
                this.Discount = PurchaseValue - this.Total;
            }

            else
            {
                this.PurchaseValue = purchaseValue;
                this.Total = PurchaseValue - (PurchaseValue * this.DiscountRate);
                this.DiscountRate *= 100;
                this.Discount = PurchaseValue - this.Total;
            }

            sb.AppendLine($"Purchase value: ${this.PurchaseValue:f2}");
            sb.AppendLine($"Discount rate: {this.DiscountRate:f1}%");
            sb.AppendLine($"Discount: ${this.Discount:f2}");
            sb.AppendLine($"Total: ${this.Total:f2}");

            return sb.ToString();
        }
    }
}
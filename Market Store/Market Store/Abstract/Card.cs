﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Market_Store
{
    public abstract class Card
    {
        //Fields
        private double purchaseValue,
        turnOver,
        discountRate,
        discount,
        total;

        //Constructor
        public Card()
        {
            this.PurchaseValue = purchaseValue;
            this.TurnOver = turnOver;
            this.DiscountRate = discountRate;
            this.Discount = discount;
            this.Total = total;
        }

        //Properties
        public double PurchaseValue
        {
            get => this.purchaseValue;
            set
            {
                this.purchaseValue = value;
            }
        }
        public double DiscountRate
        {
            get => this.discountRate;
            set
            {
                this.discountRate = value;
            }
        }

        public double Discount
        {
            get => this.discount;
            set
            {
                this.discount = value;
            }
        }
        public double Total
        {
            get => this.total;
            set
            {
                this.total = value;
            }
        }

        public double TurnOver
        {
            get => this.turnOver;
            set
            {
                this.turnOver = value;
            }
        }

        //Methods 
        public abstract string DiscountByCard(double turnOver, double purchaseValue);
    }
}
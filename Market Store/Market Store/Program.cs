﻿using System;

namespace Market_Store
{
    class Program
    {
        static void Main(string[] args)
        {
            var bronzeDiscount = new BronzeCard();

            Console.WriteLine(bronzeDiscount.DiscountByCard(0, 150));

            var silverDiscount = new SilverCard();

            Console.WriteLine(silverDiscount.DiscountByCard(600, 850));

            var goldDiscount = new GoldCard();

            Console.WriteLine(goldDiscount.DiscountByCard(1500, 1300));
        }
    }
}
